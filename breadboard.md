# Platine d'expérimentation

source:
https://fr.wikipedia.org/wiki/Platine_d%27exp%C3%A9rimentation

## Dédinition
Une platine d'expérimentation ou platine de prototypage (appelée en anglais breadboard, solderless breadboard, protoboard, plugboard ou encore Labdec du nom de la marque la plus répandue) est un dispositif qui permet de réaliser le prototype d'un circuit électronique et de le tester. L'avantage de ce système est d'être totalement réutilisable, car il ne nécessite pas de soudure.

