# Résistance

## Sources :
https://fr.wikipedia.org/wiki/R%C3%A9sistance_(composant)
http://www.dcode.fr/code-couleur-resistance

## Définition
Une résistance est un composant électronique ou électrique dont la principale caractéristique est d'opposer une plus ou moins grande résistance (mesurée en ohms) à la circulation du courant électrique.

Code des couleurs pour les résistances

|    |1er</BR>anneau</BR>gauche|2ème</BR>anneau</BR>gauche|3ème</BR>anneau</BR>gauche|Dernier</BR>anneau</BR>gauche|
|----|:-----------------:|:--------------------------:|:--------------------------:|:-----------------------------:|
|Couleur|1er chiffre|2ème chiffre|3ème chiffre|multiplicateur|
|Noir|0|0|0|x1|
|Marron|1|1|1|x10|
|Rouge|2|2|2|x100|
|Orange|3|3|3|x1 000(Kilo Ohms|
|Jaune|4|4|4|x10 000|
|Vert|5|5|5|x100 000|
|Bleu|6|6|6|x1 000 000 (Mega Ohms|
|Violet|7|7|7|x10 000 000|
|Gris|8|8|8|x100 000 000|
|Blanc|9|9|9|x1 000 000 000(Giga Ohms|
|Or||||0,1|
|argent||||0,01|

## Exercice
Calculer la resistance des composants suivants:
