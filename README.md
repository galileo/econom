# econom

Comment rendre une un batiment publique plus économe en énergie?

## Le contexte
La municipalité constate que la facture énergétique des bâtiment est très importante.

## Eléments de réflexion
Pour mener à bien un projet de réduction de la dépense énergétique, nous pouvons nous appuyer sur plusieurs éléments

## Métrologie

### éléments simples et peu couteux
 - le relevé téléinformation EDF : raspberry pi + emoncms + teleinfuse

Cette équipement connecté à la prise téléinfo d'un compteur EDF permet d'obtenir la consomation en temps réél
Cette mesure permet de vérifier que la puissance choisie correspond bien aux besoins et permet de vérifier l'efficacité des outils pour lisser la consommation.

**environ 70€**

chargeur de téléphone de récupération 5€ en moins

boitier en LEGO : 5€ en moins

carte sd de récupération 12€ en moins 

![teleinfo](https://github.com/nvallas/econom/blob/master/teleinfoEtDs18b20.png)

 - La température des pièces : sondes des températures esp8266+ds18b20

Ces sondes placées dans différentes pièces permet de mesurer les écarts entre une température confort et une température mesurée.

esp8266 : 6€

ds18b20 : 4€

batterie : 5€

boitier : 1,50€

cables : 0,50€

**Total 17€**

![sonde](https://github.com/nvallas/econom/blob/master/sonde%20temp%C3%A9rature.png)

 - Température extérieure : sonde de température ou script sur un site météo

### éléments plus complexes et plus couteux
- relevé de consommation avec des pinces ampèremétriques

https://shop.openenergymonitor.com/emontx-v3-electricity-monitoring-transmitter-unit-433mhz/
 
Permet de mesurer la consommation de sous éléments dans un batiment (chauffages, appareils électriques, etc)
 
 - Prévision de température de la nuit/lendemain

Permet d'optimiser le chauffage en utilisant l'inertie du local à chauffer

- détection de la luminosité

permet de détecter les lumières oubliées
 
 - variations de température dans la journée
 
Un chauffage optimum permet d'avoir une température régulière dans la pièces
 
Des variations importantes indiquent un défaut dans la régulation

 - ouvertures des portes extérieures intérieures
 
Oublier de fermer une porte peut entraîner une variation importante de température
